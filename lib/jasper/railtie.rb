require 'erb'
require 'yaml'

module Jasper
  class Railtie < ::Rails::Railtie
  
    # This will load the configuration of the Jasper Server
    initializer 'jasper.initialize_connection' do |app|
      ActiveSupport.on_load(:jasper) do
        unless ENV['JASPER_URL']
          self.configurations = YAML::load(
            ERB.new(IO.read("#{Rails.root}/config/jasper.yml")).result)
        end
        self.establish_connection
      end
    end
    # Now we need to have it load any non-loaded classes to configure them.
    
  end
  
end