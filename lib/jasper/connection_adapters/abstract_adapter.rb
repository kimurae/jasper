# Mostly to provide the defaults that make it work with the connection pool code.
module Jasper
  module ConnectionAdapters
    class AbstractAdapter
      include ::ActiveSupport::Callbacks
      include MonitorMixin
      
      define_callbacks :checkout, :checkin
      
      attr_accessor :visitor, :pool
      attr_reader :schema_cache, :last_use, :in_use, :logger
      alias :in_use? :in_use
      
       def initialize(connection, logger = nil, pool = nil) #:nodoc:
        super()

        @connection          = connection
        @in_use              = false
        @instrumenter        = ActiveSupport::Notifications.instrumenter
        @last_use            = false
        @logger              = logger
        @pool                = pool
        @visitor             = nil
      end


      def clear_cache!
    
      end
  
      # Have to figure out how this works.
      def active?
        true
      end
      
      def expire
        @in_use = false
      end
      
      # Mostly for connection pooling.
      def lease
        synchronize do
          unless in_use
            @in_use   = true
            @last_use = Time.now
          end
        end
      end
      # Checks whether the connection to the database is still active (i.e. not stale).
      # This is done under the hood by calling <tt>active?</tt>. If the connection
      # is no longer active, then this method will reconnect to the database.
      def verify!(*ignored)
        reconnect! unless active?
      end
      
      
    end
  end
end