require 'savon'

module Jasper
  # Reopens ConnectionHandling, and adds the adapter method.
  module ConnectionHandling
    def jasper45_connection(config)
      conn_params = config.symbolize_keys
      ConnectionAdapters::Jasper45Adapter.new(nil, logger, conn_params, config) 
    end
  end
  
  module ConnectionAdapters
  # Jasper 4.5 adapter, uses SOAP because REST isn't fully
  # Supported until Jasper 5.
  
    class Jasper45Adapter < AbstractAdapter

      
      ADAPTER_NAME = 'Jasper 4.5'
  
      def adapter_name
        ADAPTER_NAME
      end
      
      # Initializes and connects to a Jasper 4.5 instance.
      # In this case it is a Savon Soap connection.
      def initialize(connection, logger, connection_parameters, config)
        super(connection, logger )
        @client = Savon::Client.new {
          wsdl.document = connection_parameters[:wsdl] || "#{Rails.root}/config/jasper/#{Rails.env}.wsdl"
          http.auth.basic connection_parameters[:username], connection_parameters[:password]
        }
        @schema_cache = { }
      end
  
      
      
      # Institutes a repository get of the report.
      # Returns a report object?
      def get(uriString, resc = 'jrxml')    
        report_schema(uriString)[resc].map do |u|      
          Nokogiri::XML(
            case resc
            when 'jrxml'
              attachments( 
                request(:get) do |xml| 
                  xml.resourceDescriptor( :uriString => u )
                end 
              )[:attachment]
            when 'inputControl'
              request(:get) do |xml| 
                  xml.resourceDescriptor( :uriString => u )
                end.to_hash[:get_response][:get_return]
            end)           
        end       
      end
      
      def run_report(uriString, fmt = 'HTML', params = {})
        response = request(:run_report) do |xml|
          xml.argument(fmt, :name => "RUN_OUTPUT_FORMAT")
          xml.resourceDescriptor(:uriString => uriString) do
            xml.label
            params.select { |k,v| v }.each do |k,v|
              [v].flatten.compact.each do |i|
                if v.respond_to?(:map)
                  xml.parameter( "#{i}", :name => "#{k}", :isListItem => "true")
                else
                  xml.parameter( "#{i}", :name => "#{k}")
                end
              end # [v]
            end # params
          end
        end
        
        # Because Jasper returns HTML and PDF in a special way.
        # If we get classes, we should strip out style as well.
        case fmt 
        when 'HTML'
          crap = attachments(response)
          doc   = Nokogiri::HTML(crap[:report])
          
          # Jasper puts charts as attachments to the response.
          # hence lets put the data as a data-uri in image.
          doc.search('img').each do |img|
            src = Base64.encode64(crap[:"#{/images\/([^\s]+)$/.match(img['src'])[1]}" ]) rescue nil
            img['src'] = "data:image/png;base64,#{src}"
          end
          
          doc.search('body').first.traverse do |n|
            # raise "#{n}"
            if n['style']
              n['class'] = n['style'].split(/;\s*/).map { |s| s.gsub(/\W+/,'-') }.join(' ')
              n['style'] = ''
            end
          end
          
          begin
            @report_output = "#{doc.search('td')[1].children}".html_safe
          rescue
            err = Nokogiri::XML(response.to_hash[:run_report_response][:run_report_return])
            code = err.search('returnCode').first.text
            mesg = err.search('returnMessage').first.text
            raise JasperServerError, "JASPER ERROR #{code}: #{mesg}"
          end
        # when 'PDF'
          # response.http.raw_body
        else
          attachments(response)[:report]
        end
      end
      
      private
      
      # Returns a hash of attachments with the content-id as the key.
      def attachments(response)
        response.http.raw_body.split(/------=_Part[^\n\r]+[\n\r$]/).inject({}) do |h,v|
          if (k = /Content-Id: <([^\r\n]+)>[\r\n]$/.match(v)[1] rescue nil)
            h.merge(:"#{k}" => v.lines.to_a[5..-1].join )
          else
            h
          end
        end
      end
      
      def request(operation, &block)
        @client.request(operation) do
          soap.body do |xml|
            xml.requestXmlString do |r|
              r.cdata! "#{ Nokogiri::XML::Builder.new do |xml|
                          xml.request(:operationName => "#{operation}") do
                            yield xml
                          end
                        end.to_xml}" 
            end
          end
        end
      end
      
      # Fetches the schema of the report which lists its contents
      # including the jrxml and input controls.
      # loads and returns a hash of arrays of uris pointing to resources.
      def report_schema(uriString)
        @schema_cache[:"#{uriString}" ] ||= Nokogiri::XML::Document.parse(
          request(:get) do |xml2| 
            xml2.resourceDescriptor(:uriString => uriString) 
          end.to_hash[:get_response][:get_return]).search('resourceDescriptor').inject({}) do |h,i|
            if i['uriString'] && ( k = i['wsType'] )
              h[k] ||= []
              h[k] << i['uriString']
            end
            h
          end
      end
      
    end
  
  # I don't think we need reconnect, and disconnect.
  end
end
