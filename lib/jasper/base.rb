require 'jasper/connection_adapters/connection_pool'
require 'jasper/connection_adapters/connection_specification'
require 'jasper/connection_handling'

module Jasper
  # Base class for Jasper Models.
  class Base
    include ::ActiveModel::Conversion
    include ::ActiveModel::Validations
    extend  ::ActiveModel::Naming
    extend  ::ActiveModel::Translation
    extend  ConnectionHandling  # Handles Configuration and the Jasper Connection.
    
    mattr_accessor :configurations, instance_writer: false
    self.configurations = {}
    
    class_attribute :connection_handler, instance_writer: false
    self.connection_handler = ConnectionAdapters::ConnectionHandler.new
  
    # Accepts a logger conforming to the interface of Log4r or the default Ruby 1.8+ Logger class,
    # which is then passed on to any new database connections made and which can be retrieved on both
    # a class and instance level by calling +logger+.
    cattr_accessor :logger, :instance_writer => false
    
    # ActiveModel likes a method that returns an attributes hash
    # this attributes hash happens to be based off parameters set
    # in the jasper report.
    # Also useful in serialization.
    def attributes
      @attributes = self.class.attributes.keys.inject({}) { |h,k| h.merge!( k => self.send(k) ) }
    end
    
    # Like ActiveRecord, allows you to pass a hash of attributes
    # on creation.
    def initialize(attributes = {})
      super()
      attributes.each do |k,v|
        # Clear out "" from attributes.
        v.reject! { |i| "#{i}".empty? } if v.respond_to?(:reject!)
        self.send(:"#{k}=", v)
      end
    end
    
    # This runs the report and returns the response
    # in the format specified.
    def run(fmt = 'HTML')
      response = Jasper::Base.connection.run_report(self.class.uri_string, fmt, self.attributes)
    end
    
    def self.attributes
      @attributes || {}
    end
    
    # TODO: This should really just become an attribute
    # and allow the activemodel attribute module
    # do the rest.
    def self.attribute_methods_generated?
      @attribute_methods_generated = false unless 
      @attribute_methods_generated
    end
    
    # Naming is from active record, but I don't cache a schema since there
    # is no information_schema in Jasper, I could crawl through the repo,
    # but I currently do not see the need to do so, hence at this point
    # it asks for the JRXML file and reads the parameter list.
    # TODO: Right now I'll have it do the slow way of grabbing the jrxml. 
    # But eventually I will cache it.
    def self.define_attribute_methods
      doc = Jasper::Base.connection.get(self.uri_string).first
      @attributes = doc.search('parameter').inject({}) do |h,i|
        h = if i['name'] 
              h.merge!(:"#{i['name']}" => nil)
            else
              h
            end
      end
      @attributes.keys.each do |a|
        self.send(:attr_accessor, a)
      end
      # Input Controls
      @attribute_methods_generated = true
    end
    

    
    # The repository URI for the report.
    # Able to be overridden.
    # Should translate namespace as directories.
    def self.uri_string(value = nil)
      self.prefix << ( @uri_string = value || @uri_string || self.name.underscore )
    end
    
    # Grabbed from Active Record.
     # If we haven't generated any methods yet, generate them, then
    # see if we've created the method we're looking for.
    # Might be a little slower than ActiveRecord's because I can't find
    # all the haxies they did, but its a bit clearer.
    def method_missing(method, *args, &block) # :nodoc:
      unless self.class.attribute_methods_generated?
        self.class.define_attribute_methods 
        return self.send(method, *args, &block) if self.respond_to?(method)
      end
      super
    end
    
    # This also allows us to define attribute methods before checking
    # to see if the object responds to the method.
    def respond_to?(name, include_private = false)
      self.class.define_attribute_methods unless self.class.attribute_methods_generated?
      super
    end  
    
    # Jasper models are not in a database.
    def persisted?
      false
    end  
    
    protected
    # This sets the prefix for the report uri string, assuming Jasper is setup with
    # root -> organizations -> organization -> Reports.
    # If the prefix is different you will have to override it.
    def self.prefix
      "/organizations/#{self.connection_config[:organization]}/Reports/"
    end
  end
end