module Jasper

  class JasperError < StandardError; end
  
  #Raised when adapter not specified on connection (or configuration file <tt>config/jasper.yml</tt>
  # misses adapter field).
  class AdapterNotSpecified < JasperError; end

  class AdapterNotFound < JasperError; end
  
  class ConnectionNotEstablished < JasperError; end
  
  class JasperServerError < JasperError; end
end