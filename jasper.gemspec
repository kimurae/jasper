$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "jasper/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'jasper'
  s.version     = Jasper::VERSION
  s.authors     = ['Jason Kenney']
  s.email       = ['bloodycelt@gmail.com']
  s.homepage    = 'http://www.github.com/bloodycelt/jasper'
  s.summary     = 'ActiveRecord inspired connection interface for Jasper.'
  s.description = 'ActiveRecord inspired connection interface for Jasper.'

  s.files = [
    'lib/jasper.rb',
    'lib/jasper/base.rb',
    'lib/jasper/connection_adapters/abstract_adapter.rb',
    'lib/jasper/connection_adapters/connection_pool.rb',
    'lib/jasper/connection_adapters/jasper45_adapter.rb',
    'lib/jasper/connection_handling.rb',
    'lib/jasper/errors.rb',
    'lib/jasper/railtie.rb',
    'lib/jasper/version.rb',
    'MIT-LICENSE', 
    'Rakefile', 
    'README.rdoc'
  ]
  
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2"
  s.add_dependency 'savon', "~> 1.2"
  
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
end
