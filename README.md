jasper
======

Updated gem that will allow you to generate reports from Jasper in HTML, PDF, or some other format.

Currently in alpha, not production ready.

Postmortem
--------------
The company I work for requested a few reports that could be rendered in PDF, HTML, and XLS. 
Hence, Japser was already being used by another project so after looking at the largely Ruby 1.8/Rails 2
gems, I had to make one for Ruby 1.9/Rails 3. I decided to try and make it feel like ActiveRecord/Resource
and used a subststantial amount of code to cache the connection to jasper, and have similar dev/qa/production
configuration. But in the end, despite the code working, and staying relatively clean, Japser is not meant
to be a back end report server, its a front-end report server. Hence why I leave this code here for educational
purposes, but aside from supporting the one production site using this code, its a dead-end. And when I can
I will advocate for that one production site to use a different solution. So I will not have Installation instructions
or Usage, because I don't support anyone actually using this code.

